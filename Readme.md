MODULE GIT
---
![Git Icon for Git Module](/git.png "Version control with Git")

**Commandes utilisées dans le cours**
<details>
<summary>Les commandes de base</summary>
<br>

- `git clone`=Cloner un dépôt <br>

- `git status`=Afficher le statut du dépôt local
- `git add`= Met les fichiers à la phase "stage". Git suit désormais ces fichiers
- `git config --global user.name "user"`=Forunit un nom d'utilisateur pour le dépôt local
- `git config --global user.email "email"`=Forunit un mail pour le dépôt local
- `git commit`=Met tout ce qui est à la phase "stage" à la phase "dépôt local", avec un descriptif
- `git push`=Envoie tout ce qui est à la phase "dépôt local" sur le "dépôt distant"
- `git log`=Affiche les logs de commit
- `git init`=Initialisation d'un dépôt local
- `git remote add`=Connexion avec le dépôt distant
- `git push --set-upstream origin`=Push de la branche
- `git pull`=Récupère les fichiers sur le dépôt distant <br>

</details>

<details>
<summary>Les branches</summary>
<br>

- `git branch`=Liste les branches <br>

- `git branch -a`=Liste toutes les branches <br>
- `git checkout`=Changer de branches <br>
- `git checkout -b`=Créer une branche localement et switcher <br>
- `git branch -d`=Supprimer une branche <br>
- `git merge`=Fait un merge de branches <br>

</details>

<details>
<summary>Git Rebase</summary>
<br>

- `git pull -r`=Pull sans commit de merge <br>

- `git rebase --continue`=Restructurer après un merge conflict <br>

</details>

<details>
<summary>.gitignore</summary>
<br>

- `git rm -r --cached`=Retire un dossier du tracking de git <br>

</details>

<details>
<summary>Git stash</summary>
<br>

- `git stash`=Enregistre un travail pour plus tard <br>

- `git stash pop`=Ramène le travail mis en stash <br>


</details>

<details>
<summary>Git - Historique</summary>
<br>

- `git log`=Affiche les logs de commit <br>

- `git checkout <commit_hash>`=Retourne au moment du commit précisé <br>


</details>

<details>
<summary>Git - Revert Commit</summary>
<br>

- `git reset --hard HEAD~n`=Supprime le commit et les modifications <br>
_n_ indique le nombre de commit qu'on souhaite supprimer <br>

- `git reset --soft HEAD~n`=Supprime le commit et conserve les modifications<br>

- `git commit --amend`=Ajoute les modifications au commit précédent <br>

- `git push --force`=Pousser sur le dépôt distant malgré le warning <br>

</details>
